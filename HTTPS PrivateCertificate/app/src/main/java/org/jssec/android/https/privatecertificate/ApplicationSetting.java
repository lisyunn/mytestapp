package org.jssec.android.https.privatecertificate;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import java.util.Stack;

import AnalysisLog.AnalysisLogExceptionHandler;


/**
 * 自アプリのActivityが最前面にいるかを判別する.
 * またonPause呼出時に全リソース解放が必要なデバイスにおいてリソース解放を行う.
 */
public class ApplicationSetting extends Application {
    private static Context mContext;
    private static int mCount;
    private Stack<Activity> mActivities = new Stack<Activity>();
    // ActivityLifecycleCallbacksインターフェイス型のフィールドを宣言する
    private ActivityLifecycleCallbacks activityLifecycleCallbacks = new ActivityLifecycleCallbacks() {
        /**
         * ActivityのLifecycleが変わった時に呼ばれるinterface
         */
        @Override
        public void onActivityCreated(final Activity activity, final Bundle savedInstanceState) {
            mActivities.push(activity);

        }

        @Override
        public void onActivityStarted(final Activity activity) {
            Log.d(" @@@ onActivityStarted " , " <<" + activity.getClass().getName() + ">> ");
            mCount++;
        }

        @Override
        public void onActivityResumed(final Activity activity) {
            Log.d(" @@@ onActivityResumed " , " <<" + activity.getClass().getName() + ">> ");
        }

        @Override
        public void onActivityPaused(final Activity activity) {
            Log.d(" @@@ onActivityPaused " , " <<" + activity.getClass().getName() + ">> ");
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final String myPackageName = getPackageName();
                        // <uses-permission android:name="android.permission.GET_TASKS" /> パーミッションが必要
                        final String topPackageName = getTopPackageName();
                        if (!topPackageName.isEmpty() && !myPackageName.contentEquals(topPackageName)) {
                            Log.d("", " @@@ [finishAll] myPackageName:" + myPackageName + " topPackageName:" + topPackageName);
                            finishAll();
                        }
                    }
                }, 1000);
        }

        @Override
        public void onActivityStopped(final Activity activity) {
            Log.d(" @@@ onActivityStopped " , " <<" + activity.getClass().getName() + ">> ");
            mCount--;
        }

        @Override
        public void onActivitySaveInstanceState(final Activity activity, final Bundle outState) {
        }

        @Override
        public void onActivityDestroyed(final Activity activity) {
            Log.d("", " @@@ onActivityDestroyed " + " <<" + activity.getClass().getName() + ">> ");
            mActivities.remove(activity);
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
//        // 解像度がxhdpi以下の場合は、強制的に解像度を変更する
//        if (getApplicationContext().getResources().getDisplayMetrics().density < 2) {
//            getApplicationContext().getResources().getDisplayMetrics().density = 2;
//        }
        // ActivityのLifecycleを登録する
        registerActivityLifecycleCallbacks(activityLifecycleCallbacks);
        // 初期化
        mCount = 0;


        if(!(Thread.getDefaultUncaughtExceptionHandler() instanceof AnalysisLogExceptionHandler)) {
            Thread.setDefaultUncaughtExceptionHandler(new AnalysisLogExceptionHandler(
                    this));
        }
    }

    public static Context getContext() {
        return mContext;
    }

    /**
     * 自分のアプリが前面にいるのか
     * @return true:いる、false:いない
     */
    public static boolean isApplicationVisible() {
        if(mCount > 0) {
            return true;
        }
        return false;
    }

    @Override
    public void onTerminate() {
        // ActivityのLifecycleを解除する
        unregisterActivityLifecycleCallbacks(activityLifecycleCallbacks);
        if(!mActivities.isEmpty()) {
            mActivities.clear();
        }
        super.onTerminate();
    }

    /**
     * STB向けAndroidアプリがバックグラウンドかを検知する
     * @return フォアグラウンドパッケージ名
     */
    private String getTopPackageName() {
        String name = "";
        try {
            final ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
            if (am != null) {
                final ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
                if (cn != null) {
                    name = cn.getPackageName();
                }
            }
        } catch (Exception e) {
            // nop
        }
        return name;
    }

    /**
     * STB向けAndroidアプリ内すべてのActivity終了させる
     */
    private void finishAll() {
        if (mActivities.isEmpty()) {
            return;
        }
        final Stack<Activity> stack = new Stack<Activity>();
        stack.addAll(mActivities);
        mActivities.clear();
        while (!stack.isEmpty()) {
            final Activity activity = stack.pop();
            Log.d("", " @@@ [finishAll] ClssName:" + activity.getClass().getName());
            activity.finish();
        }
    }
}