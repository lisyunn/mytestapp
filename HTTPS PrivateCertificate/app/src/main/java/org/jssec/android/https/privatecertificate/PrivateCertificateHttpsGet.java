/*
 * Copyright (C) 2012-2016 Japan Smartphone Security Association
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jssec.android.https.privatecertificate;

import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLSession;

//import org.bouncycastle.ocsp.BasicOCSPResp;
//import org.bouncycastle.ocsp.CertificateID;
//import org.bouncycastle.ocsp.CertificateStatus;
//import org.bouncycastle.ocsp.OCSPException;
//import org.bouncycastle.ocsp.OCSPReq;
//import org.bouncycastle.ocsp.OCSPReqGenerator;
//import org.bouncycastle.ocsp.OCSPResp;
//import org.bouncycastle.ocsp.OCSPRespStatus;
//import org.bouncycastle.ocsp.SingleResp;

//import javax.net.ssl.SSLException;
//import javax.net.ssl.TrustManagerFactory;

public abstract class PrivateCertificateHttpsGet extends AsyncTask<String, Void, Object> {

    private Context mContext;
    private final int BUFFER_SIZE = 1024;
    private final boolean isHttpClient = false;

    public PrivateCertificateHttpsGet(Context context) {
        mContext = context;
    }

    @Override
    protected Object doInBackground(String... params) {
        //TrustManagerFactory trustManager;
        BufferedInputStream inputStream = null;
        ByteArrayOutputStream responseArray = null;
        byte[] buff = new byte[BUFFER_SIZE];
        int length;
        HttpURLConnection con = null;
        URL url = null;
        try {
            url = new URL(params[0]);

            if(isHttpClient) {
                int timeout = 5000;
                DefaultHttpClient client = new DefaultHttpClient();
                HttpParams httpParams = client.getParams();
                HttpConnectionParams.setStaleCheckingEnabled(httpParams, false);
                HttpConnectionParams.setSocketBufferSize(httpParams, 8192);
                HttpClientParams.setRedirecting(httpParams, false);
                HttpConnectionParams.setSoTimeout(httpParams, timeout);
                HttpConnectionParams.setConnectionTimeout(httpParams, timeout);
                HttpRequestBase mHttpRequest = createRequest(params[0], 2);
                HttpResponse httpResponse = client.execute(mHttpRequest);
                final HttpEntity entity = httpResponse.getEntity();
                if (entity != null) {
                    InputStream in = null;
                    OutputStream out = null;
                    try {
                        in = entity.getContent();
                        responseArray = new ByteArrayOutputStream();
                        out = new BufferedOutputStream(responseArray, BUFFER_SIZE);
                        byte[] b = new byte[BUFFER_SIZE];
                        int read;
                        while ((read = in.read(b)) != -1) {
                            out.write(b, 0, read);
                        }
                        out.flush();


                    } finally {
                        entity.consumeContent();
                    }
                }
            }
            else {

//            // ★ポイント1★ プライベート証明書でサーバー証明書を検証する
//            // assetsに格納しておいたプライベート証明書だけを含むKeyStoreを設定
//            KeyStore ks = KeyStoreUtil.getEmptyKeyStore();
//            KeyStoreUtil.loadX509Certificate(ks,
//                    mContext.getResources().getAssets().open("cacert.crt"));


                if(false) {
                    // ホスト名の検証を行う
                    HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            if (!hostname.equals(session.getPeerHost())) {
                                return false;
                            }
                            return true;
                        }
                    });
                }

//            // ★ポイント2★ URIはhttps://で始める
//            // ★ポイント3★ 送信データにセンシティブな情報を含めてよい
//            trustManager = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
//            trustManager.init(ks);
//            SSLContext sslCon = SSLContext.getInstance("TLS");
//            sslCon.init(null, trustManager.getTrustManagers(), new SecureRandom());



                con = (HttpURLConnection) url.openConnection();
                HttpsURLConnection response = (HttpsURLConnection) con;
                //HttpURLConnection response = con;
                //System.setProperty("http.agent","cccc");
                //con.setRequestProperty("User-Agent", System.getProperty("http.agent"));
                //con.setRequestProperty("User-Agent", "bbbb");
                String userAgent = con.getRequestProperty("User-Agent");
                con.connect();




//            response.setDefaultSSLSocketFactory(sslCon.getSocketFactory());
//            response.setSSLSocketFactory(sslCon.getSocketFactory());

                //checkResponse(response);

//            Certificate[] e = response.getServerCertificates();
//            X509Certificate peerX509cert = (X509Certificate)e[0];
//            X509Certificate caX509cert = (X509Certificate)e[1];
//            checkRevocationStatus(peerX509cert, caX509cert);


                // ★ポイント4★ 受信データを接続先サーバーと同じ程度に信用してよい
                inputStream = new BufferedInputStream(response.getInputStream());
                responseArray = new ByteArrayOutputStream();
                while ((length = inputStream.read(buff)) != -1) {
                    if (length > 0) {
                        responseArray.write(buff, 0, length);
                    }
                }

            }
            //Log.d("My app", "User agent = " + con.getRequestProperty("User-Agent"));
            //Log.d("My app", "User agent = " + con.getHeaderField("User-Agent"));
            return responseArray.toByteArray();

        } catch(SSLException e) {
            // ★ポイント5★ SSLExceptionに対しユーザーに通知する等の適切な例外処理をする
            // サンプルにつき例外処理は割愛

            return e;
//        } catch(IOException e) {
//            String hostname = url.getHost();
//            String message = e.getMessage();
//            String msg = "Hostname" + " '" + hostname + "' " + "was not verified";
//            //if(e.getMessage().contains("Hostname") && e.getMessage().contains("was not verified")) {
//            if(e.getMessage().contentEquals(msg)) {
//                try {
//                    throw new SSLException(e.getMessage());
//                } catch (SSLException ex) {
//                    return ex;
//                }
//            }
//            else {
//                return e;
//            }

        } catch(Exception e) {
            return e;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e) {
                    // 例外処理は割愛
                }
            }
            if (responseArray != null) {
                try {
                    responseArray.close();
                } catch (Exception e) {
                    // 例外処理は割愛
                }
            }
            if(con != null) {
                con.disconnect();
            }
        }
    }

    private HttpRequestBase createRequest(String url, int method) {
        if (method == 1) {
            return new HttpGet(url);
        } else if (method == 2) {
            return new HttpPost(url);
        } else if (method == 3) {
            return new HttpHead(url);
        }
        return null;
    }

    private void checkResponse(HttpURLConnection response) throws IOException {
        int statusCode = response.getResponseCode();

        if (HttpURLConnection.HTTP_OK != statusCode) {
            throw new IOException("HttpStatus: " + statusCode );
        }
    }

//    /**
//     * This method generates an OCSP Request to be sent to an OCSP endpoint.
//     *
//     * @param issuerCert   is the Certificate of the Issuer of the peer certificate we are interested in.
//     * @param serialNumber of the peer certificate.
//     * @return generated OCSP request.
//     * @throws CertificateVerificationException
//     */
//    private OCSPReq generateOCSPRequest(X509Certificate issuerCert, BigInteger serialNumber)
//            throws CertificateVerificationException {
//
//        //Add provider BC
//        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
//        try {
//            //  CertID structure is used to uniquely identify certificates that are the subject of
//            // an OCSP request or response and has an ASN.1 definition. CertID structure is defined in RFC 2560
//            CertificateID id = new CertificateID(CertificateID.HASH_SHA1, issuerCert, serialNumber);
//
//            // basic request generation with nonce
//            OCSPReqGenerator generator = new OCSPReqGenerator();
//            generator.addRequest(id);
//
//            // create details for nonce extension. The nonce extension is used to bind
//            // a request to a response to prevent replay attacks. As the name implies,
//            // the nonce value is something that the client should only use once within a reasonably small period.
//            BigInteger nonce = BigInteger.valueOf(System.currentTimeMillis());
//            Vector objectIdentifiers = new Vector();
//            Vector values = new Vector();
//
//            //to create the request Extension
//            objectIdentifiers.add(OCSPObjectIdentifiers.id_pkix_ocsp_nonce);
//            values.add(new X509Extension(false, new DEROctetString(nonce.toByteArray())));
//            generator.setRequestExtensions(new X509Extensions(objectIdentifiers, values));
//
//            return generator.generate();
//        }
//        catch (OCSPException e) {
//            e.printStackTrace();
//            throw new CertificateVerificationException("Cannot generate OSCP Request with the given certificate",e);
//        }
//    }
//
//    /**
//     * Gets an ASN.1 encoded OCSP response (as defined in RFC 2560) from the given service URL. Currently supports
//     * only HTTP.
//     *
//     * @param serviceUrl URL of the OCSP endpoint.
//     * @param request    an OCSP request object.
//     * @return OCSP response encoded in ASN.1 structure.
//     * @throws CertificateVerificationException
//     *
//     */
//    protected OCSPResp getOCSPResponse(String serviceUrl,
//                                       OCSPReq request) throws CertificateVerificationException {
//        try {
//            //Todo: Use http client.
//            byte[] array = request.getEncoded();
//            if (serviceUrl.startsWith("http")) {
//                HttpURLConnection con;
//                URL url = new URL(serviceUrl);
//                con = (HttpURLConnection) url.openConnection();
//                con.setRequestProperty("Content-Type", "application/ocsp-request");
//                con.setRequestProperty("Accept", "application/ocsp-response");
//                con.setDoOutput(true);
//                OutputStream out = con.getOutputStream();
//                DataOutputStream dataOut = new DataOutputStream(new BufferedOutputStream(out));
//                dataOut.write(array);
//
//                dataOut.flush();
//                dataOut.close();
//
//                //Check errors in response:
//                if (con.getResponseCode() / 100 != 2) {
//                    throw new CertificateVerificationException("Error getting ocsp response." +
//                            "Response code is " + con.getResponseCode());
//                }
//
//                //Get Response
//                InputStream in = (InputStream) con.getContent();
//                return new OCSPResp(in);
//            } else {
//                throw new CertificateVerificationException("Only http is supported for ocsp calls");
//            }
//        } catch (IOException e) {
//            throw new CertificateVerificationException("Cannot get ocspResponse from url: " + serviceUrl, e);
//        }
//    }
//
//
//    public void checkRevocationStatus(X509Certificate peerCert, X509Certificate issuerCert)
//            throws CertificateVerificationException {
//
//        try {
//
//            OCSPReq request = generateOCSPRequest(issuerCert, peerCert.getSerialNumber());
//            List<String> locations = getAIALocations(peerCert);
//            Iterator it = locations.iterator();
//
//            if (it.hasNext()) {
//
//                String serviceUrl = (String) it.next();
//                OCSPResp ocspResponse = getOCSPResponse(serviceUrl, request);
//                if(OCSPRespStatus.SUCCESSFUL==ocspResponse.getStatus())
//                    System.out.println("server gave response fine");
//
//                BasicOCSPResp basicResponse = (BasicOCSPResp) ocspResponse.getResponseObject();
//                SingleResp[] responses = (basicResponse==null) ? null : basicResponse.getResponses();
//
//                if (responses!=null && responses.length == 1) {
//                    SingleResp resp = responses[0];
//                    Date data1 = resp.getThisUpdate();
//                    Date data2 = resp.getNextUpdate();
//                    Object status = resp.getCertStatus();
//                    if (status == CertificateStatus.GOOD) {
//                        System.out.println("OCSP Status is good!");
//                    } else if (status instanceof org.bouncycastle.ocsp.RevokedStatus) {
//                        System.out.println("OCSP Status is revoked!");
//                    }  else if (status instanceof org.bouncycastle.ocsp.UnknownStatus) {
//                        System.out.println("OCSP Status is unknown!");
//                    }
//                }
//            }
//        }
//        catch (Exception e) {
//            System.out.println(e);
//        }
//    }
//
//    /**
//     * Authority Information Access (AIA) is a non-critical extension in an X509 Certificate. This contains the
//     * URL of the OCSP endpoint if one is available.
//     * TODO: This might contain non OCSP urls as well. Handle this.
//     *
//     * @param cert is the certificate
//     * @return a lit of URLs in AIA extension of the certificate which will hopefully contain an OCSP endpoint.
//     * @throws CertificateVerificationException
//     *
//     */
//    private List<String> getAIALocations(X509Certificate cert) throws CertificateVerificationException {
//
//        //Gets the DER-encoded OCTET string for the extension value for Authority information access Points
//        byte[] aiaExtensionValue = cert.getExtensionValue(X509Extensions.AuthorityInfoAccess.getId());
//        if (aiaExtensionValue == null) {
//            throw new CertificateVerificationException("Certificate doesn't have authority " +
//                    "information access points");
//        }
//        //might have to pass an ByteArrayInputStream(aiaExtensionValue)
//        ASN1InputStream asn1In = new ASN1InputStream(aiaExtensionValue);
//        AuthorityInformationAccess authorityInformationAccess;
//
//        try {
//            DEROctetString aiaDEROctetString = (DEROctetString) (asn1In.readObject());
//            ASN1InputStream asn1InOctets = new ASN1InputStream(aiaDEROctetString.getOctets());
//            ASN1Sequence aiaASN1Sequence = (ASN1Sequence) asn1InOctets.readObject();
//            authorityInformationAccess = AuthorityInformationAccess.getInstance(aiaASN1Sequence);
//        } catch (IOException e) {
//            throw new CertificateVerificationException("Cannot read certificate to get OCSP URLs", e);
//        }
//
//        List<String> ocspUrlList = new ArrayList<String>();
//        AccessDescription[] accessDescriptions = authorityInformationAccess.getAccessDescriptions();
//        for (AccessDescription accessDescription : accessDescriptions) {
//
//            GeneralName gn = accessDescription.getAccessLocation();
//            if (gn.getTagNo() == GeneralName.uniformResourceIdentifier && X509ObjectIdentifiers.ocspAccessMethod.getId().equals(accessDescription.getAccessMethod().getId())) {
//                DERIA5String str = DERIA5String.getInstance(gn.getName());
//                String accessLocation = str.getString();
//                ocspUrlList.add(accessLocation);
//            }
//        }
//        if (ocspUrlList.isEmpty()) {
//            throw new CertificateVerificationException("Cant get OCSP urls from certificate");
//        }
//
//        return ocspUrlList;
//    }
//
//    /**
//     * This class wraps an exception that could be thrown during the certificate
//     * verification process.
//     */
//    public class CertificateVerificationException extends Exception {
//        private static final long serialVersionUID = 1L;
//
//        public CertificateVerificationException(String message, Throwable cause) {
//            super(message, cause);
//        }
//
//        public CertificateVerificationException(String message) {
//            super(message);
//        }
//    }
}