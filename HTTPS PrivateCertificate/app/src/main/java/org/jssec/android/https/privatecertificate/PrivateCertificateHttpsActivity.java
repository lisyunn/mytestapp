/*
 * Copyright (C) 2012-2016 Japan Smartphone Security Association
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.jssec.android.https.privatecertificate;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.net.IDN;
import java.net.MalformedURLException;
import java.net.URL;

import AnalysisLog.AnalysisLogUtil;
import sslcert.ocsp.OcspException;
import sslcert.ocsp.OcspVerifier;

public class PrivateCertificateHttpsActivity extends Activity {

	private EditText mUrlBox;
	private TextView mMsgBox;
	private ImageView mImgBox;
	private AsyncTask<String, Void, Object> mAsyncTask ;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        mUrlBox = (EditText)findViewById(R.id.urlbox);
        mMsgBox = (TextView)findViewById(R.id.msgbox);
		mImgBox = (ImageView)findViewById(R.id.imageview);
		OcspVerifyInit(this);

		AnalysisLogUtil.initAnalysisLog(this);
    }
    
    @Override
	protected void onPause() {
    	// このあとActivityが破棄される可能性があるので非同期処理をキャンセルしておく
    	if (mAsyncTask != null) mAsyncTask.cancel(true);
		super.onPause();
	}


	/**
	 * 初期化処理
	 * @param context
	 */
	public static void OcspVerifyInit(final Context context) {
		if(context != null) {
			try {
				// OcspVerifierの初期化を実施
				OcspVerifier.init(context, "myUserAgent");
			} catch (OcspException e) {
				Log.e("",e.getMessage());
			}
		}
	}

	/**
	 * サーバ証明書失効確認 (URL指定)
	 * 指定されたURLのプロトコルがhttps以外(http/file/ftp)の場合は、
	 * 無条件にSTATUS_GOOD が返却される。
	 * 本メソッドは内部で通信処理を行うため、非UIスレッドにて実行する必要がある
	 * @param url
	 * @return 有効・失効・不明
	 */
	private static int OcspVerifyUrl(final URL url) {
		int status = 0;
		try {
			final URL convertedUrl = new URL(url.getProtocol(), IDN.toASCII(url.getHost()), url.getPort(), url.getFile());
			final String convertedUrlString = convertedUrl.toString();
			status = OcspVerifier.verifyUrl(convertedUrlString, true);
		} catch (MalformedURLException e) {
			Log.e("", e.getMessage());
		} catch (OcspException e) {
			Log.e("", e.getMessage());
		}
		return status;
	}

	/**
	 * 失効かどうか判定
	 * @param url
	 * @return true:失効、false:その他
	 */
	public static boolean isOcspRevoked(final URL url) {
		final int status = OcspVerifyUrl(url);
		if(status == OcspVerifier.STATUS_REVOKED) {
			return true;
		}
		return false;
	}

	/**
	 * エラー検知したら、ダイアログ表示
	 * @param url
	 * @param context
	 */
	public static void OcspVerifySpec(final String url, final Context context) {
		final URL myUrl;
		try {
			myUrl = new URL(url);
		} catch (MalformedURLException e) {
			return;
		}
		new AsyncTask<Void, Void, Integer>() {
			@Override
			protected Integer doInBackground(Void... params) {
				return OcspVerifyUrl(myUrl);
			}

			@Override
			protected void onPostExecute(Integer result) {
				// 失効(revoke)のみエラーを表示し、通信させないよう実装します。
				//（サーバ証明書の状態が不明の場合は何も行いません）
				Log.d("****", "result = " + result);
				if (result == OcspVerifier.STATUS_REVOKED && context != null) {

				}
			}
		}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null, null, null);
	}
    
    public void onClick(View view) {
    	String url = mUrlBox.getText().toString();
		OcspVerifySpec(url,this);

//		int a = 0;
//		int b = 3;
//		int c = b / a;  // exception test


		// test
		try {
			String s = null;
			s.toString();
		}
		catch (Exception e) {
			AnalysisLogUtil.logOut(e.getMessage(), e.fillInStackTrace());
		}

    	mMsgBox.setText(url);
    	mImgBox.setImageBitmap(null);


    	
    	// 直前の非同期処理が終わってないこともあるのでキャンセルしておく
    	if (mAsyncTask != null) mAsyncTask.cancel(true);

    	// UIスレッドで通信してはならないので、AsyncTaskによりワーカースレッドで通信する
    	mAsyncTask = new PrivateCertificateHttpsGet(this) {
			@Override
			protected void onPostExecute(Object result) {
				// UIスレッドで通信結果を処理する
				if (result instanceof Exception) {
					Exception e = (Exception)result;
					e.printStackTrace();
					mMsgBox.append("\n例外発生\n" + e.toString());
				} else {
					if(result == null) {
						return;
					}
					byte[] data = (byte[])result;
					if(data.length <= 0) {
						return;
					}
					Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
					mImgBox.setImageBitmap(bmp);
				}
			}
    	}.execute(url);	// URLを渡して非同期処理を開始
    }

	@Override
	protected void onDestroy() {
		AnalysisLogUtil.closeAnalysisLog();
		super.onDestroy();
	}
}
