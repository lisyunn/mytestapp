package AnalysisLog;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

class AnalysisLogHelper extends SQLiteOpenHelper {

    private static final int VERSION = 1;
    private static final String FILENAME   = "analysislogs.db";
    static final String TABLENAME   = "stack_trace";
    static final String ID          = "_id";
    static final String SIZE     = "stackTraceSize";
    static final String DATA        = "stackTraceData";
    private static AnalysisLogHelper INSTANCE = null;

    private AnalysisLogHelper(Context context) {
        super(context, FILENAME, null, VERSION);
    }

    public static AnalysisLogHelper getInstance(Context context) {
        if (context == null) {
            Log.e(AnalysisLogUtil.TAG, "The context cannot be null!");
            throw new IllegalArgumentException("The context cannot be null!");
        }
        if (INSTANCE == null) {
            INSTANCE = new AnalysisLogHelper(context);
        }
        return INSTANCE;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE `" + TABLENAME + "`(" +
                        " `" + ID         + "` INTEGER PRIMARY KEY," +
                        " `" + SIZE    + "` INTEGER," +
                        " `" + DATA       + "` TEXT" +
                        ");"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
