package AnalysisLog;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

public class AnalysisLogExceptionHandler implements Thread.UncaughtExceptionHandler {

    private Thread.UncaughtExceptionHandler defaultUEH;
    private Context mContext;

    /*
     * if any of the parameters is null, the respective functionality
     * will not be used
     */
    public AnalysisLogExceptionHandler(Context context) {
        this.mContext = context;
        this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
    }

    public void uncaughtException(Thread t, Throwable e) {
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        e.printStackTrace(printWriter);
        String stacktrace = result.toString();
        printWriter.close();

        if (mContext != null) {
            writeToFile(stacktrace, AnalysisLogUtil.UNCATCHED_FILE_NAME);
        }

        sendToServer();

        defaultUEH.uncaughtException(t, e);
    }

    private void writeToFile(String stacktrace, String filename) {
        try {
            final String filePath = mContext.getCacheDir().getPath() + "/" + filename;
            SharedPreferences settings = mContext.getSharedPreferences(AnalysisLogUtil.Preferences.PREFS_NAME, Context.MODE_PRIVATE);
            settings.edit().putString(AnalysisLogUtil.Preferences.FILE_PATH, filePath).apply();
            Log.d(AnalysisLogUtil.TAG, "writeToFile FilePath = " + filePath);
            BufferedWriter bos = new BufferedWriter(new FileWriter(filePath, true));
            // replace <TAB> to 4 spaces
            stacktrace = stacktrace.replaceAll(AnalysisLogUtil.TAB, AnalysisLogUtil.SPACE4);
            final String parameters = AnalysisLogUtil.getSendLogRecode(mContext, AnalysisLogUtil.UNCATCHED_ERROR);
            bos.write(parameters + stacktrace + AnalysisLogUtil.CRLF2);
            bos.flush();
            bos.close();
        } catch (Exception e) {
            Log.e(AnalysisLogUtil.TAG, e.getMessage());
        }
    }

    private void sendToServer() {
        Intent serviceIntent = new Intent(mContext, AnalysisLogService.class);
        mContext.startService(serviceIntent);
    }

}