package AnalysisLog;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * ver=1.0&ts=[yyyyMMddHHmmssfff]&dev=[端末識別子]&app=[アプリバージョン]&dn=[機種名]&type=L04&data=E0001_-1<TAB>[エラー詳細]
 */
public class AnalysisLogUtil {

    public static final String TAG = "AnalysisLogUtil";
    /**
     * UserAgent フォーマット
     */
    private static final String SEND_LOG_UA_FORMAT = "ANALYSISLOG/1.0 %s(VS; %s;Android;%s;%s);locale:%s;networkoperator:%s";
    private static final String LOG_VERSION = "1.0";

    static final String UNCATCHED_FILE_NAME = "AnalysisLogUncatched.stacktrace";
    static final String UNCATCHED_ERROR = "E0001";
    static final String DETECTED_ERROR = "E0002";
    static final String CRLF2  = "\r\n\r\n";
    static final String SPACE4  = "    ";
    static final String TAB = "\t";
    static final String DEST_SERVER = "https://ss1.xrea.com/lisyunn.s602.xrea.com/log_collect/server.php";
    static final int SEND_LOG_MAX_LEN = 5 * 1024; // MAX SIZE : 5KB

    class Preferences {
        static final String PREFS_NAME = "AnalysisLog";
        static final String FILE_PATH = "FilePath";
    }

    static AnalysisLogTable sAnalysisLogTable = null;

    /**
     * 分析ログ初期化処理
     * @param context interface
     */
    public static void initAnalysisLog(Context context) {
        if(context == null) {
            return;
        }
        sAnalysisLogTable = new AnalysisLogTable(context.getApplicationContext());
    }

    /**
     * 分析ログ送信
     */
    @SuppressWarnings("unused")
    public static void sendAnalysisLog(){
        if(sAnalysisLogTable != null) {
            sAnalysisLogTable.load();
        }
    }

    /**
     * 分析ログ保存DBクローズ
     */
    public static void closeAnalysisLog() {
        if(sAnalysisLogTable != null) {
            sAnalysisLogTable.close();
        }
    }

    static String getSendLogRecode(Context context, String errorCode) {
        if(context == null) {
            return "";
        }
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ver="); // ログのバージョン
        stringBuilder.append(LOG_VERSION);
        stringBuilder.append("&");
        stringBuilder.append("ts="); // タイムスタンプ
        stringBuilder.append(getDateFromUnixTime());
        stringBuilder.append("&");
        stringBuilder.append("dev="); // 端末識別子
        stringBuilder.append("myImei");
        stringBuilder.append("&");
        stringBuilder.append("app=VS"); // アプリバージョン
        stringBuilder.append(getVersionName(context));
        stringBuilder.append("&");
        stringBuilder.append("dn="); // 機種名
        stringBuilder.append(Build.MODEL);
        stringBuilder.append("&");
        stringBuilder.append("type="); // ログ種別
        stringBuilder.append("L04"); // エラーログ固定
        stringBuilder.append("&");
        stringBuilder.append("data=");
        stringBuilder.append(errorCode);
        stringBuilder.append(TAB);
        stringBuilder.append(getSendLogUA(context));
        stringBuilder.append(TAB);
        stringBuilder.append("\n");
        Log.d(TAG, "getSendLogRecode:" + stringBuilder.toString());
        return stringBuilder.toString(); // ログデータ
    }

    /**
     * 分析ログ送信時のUserAgent
     * @param context interface
     * @return UserAgent Format
     */
    static String getSendLogUA(Context context) {
        return String.format(Locale.US,
                SEND_LOG_UA_FORMAT,
                Build.MODEL,
                getVersionName(context),
                Build.VERSION.RELEASE,
                Build.VERSION.SDK_INT,
                getLocaleString(),
                "44010"); // networkoperator:44010固定
    }

    /**
     * ロケール情報取得処理
     *
     * @return ロケール情報文字列
     */
    private static String getLocaleString() {
        final StringBuilder buffer = new StringBuilder();
        final Locale locale = Locale.getDefault();
        final String language = locale.getLanguage();
        if (language != null) {
            buffer.append(language.toLowerCase(Locale.getDefault()));
            final String country = locale.getCountry();
            if (country != null) {
                buffer.append("-");
                buffer.append(country.toLowerCase(Locale.getDefault()));
            }
        } else {
            buffer.append("en");
        }
        return buffer.toString();
    }

    /**
     * アプリケーション バージョン情報取得
     *
     * @param context コンテキスト
     * @return バージョン情報文字列
     */
    private static String getVersionName(final Context context) {
        String versionName = "";
        try {
            versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_META_DATA).versionName;
        } catch (final Exception e) {
            Log.d(TAG, e.getMessage());
        }
        return versionName;
    }

    /**
     * タイムスタンプ（ミリ秒）
     * @return 文字列
     */
    private static String getDateFromUnixTime() {
        final long unixTime = System.currentTimeMillis();
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.JAPAN);
        return sdf.format(new Date(unixTime));
    }

    /**
     * 分析ログ出力・トレースログ
     */
    @SuppressWarnings("unused")
    public static void logOut() {
        outputLog(null, null);
    }

    /**
     * 分析ログ出力・レースログ
     *
     * @param message log message
     */
    @SuppressWarnings("unused")
    public static void logOut(String message) {
        outputLog(message, null);
    }

    /**
     * 分析ログ出力・レースログ
     *
     * @param message   log message
     * @param throwable log exception
     */
    public static void logOut(String message, Throwable throwable) {
        outputLog(message, throwable);
    }

    /**
     * 分析ログ出力
     * @param message   log message
     * @param throwable log exception
     */
    private static void outputLog(String message, Throwable throwable) {
        outputLog(DETECTED_ERROR, message, throwable);
    }

    /**
     * 分析ログ出力
     * @param errorCode error code
     * @param message   log message
     * @param throwable log exception
     */
    private static void outputLog(String errorCode, String message, Throwable throwable) {

        // ログのメッセージ部分にスタックトレース情報を付加します。
        if (message == null) {
            message = getStackTraceInfo();
        } else {
            message = getStackTraceInfo() + " " + message;
        }
        // ログを出力
        if (throwable == null) {
            Log.e(TAG, message);
            sAnalysisLogTable.store(message, errorCode);
        } else {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            throwable.printStackTrace(pw);
            Log.e(TAG, sw.toString());
            sAnalysisLogTable.store(sw.toString(), errorCode);
        }
    }

    /**
     * スタックトレースから呼び出し元の基本情報を取得。
     *
     * @return [className#methodName:lineNumber]
     */
    private static String getStackTraceInfo() {
        // 現在のスタックトレースを取得。
        // 0:VM 1:スレッド 2:getStackTraceInfo() 3:outputLog() 4:logOut()等 5:呼び出し元
        StackTraceElement element = Thread.currentThread().getStackTrace()[5];

        String fullName = element.getClassName();
        String className = fullName.substring(fullName.lastIndexOf(".") + 1);
        String methodName = element.getMethodName();
        int lineNumber = element.getLineNumber();

        return "[" + className + "#" + methodName + ":" + lineNumber + "]";
    }
}
