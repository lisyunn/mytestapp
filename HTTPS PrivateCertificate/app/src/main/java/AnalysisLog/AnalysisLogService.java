package AnalysisLog;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import java.io.File;

public class AnalysisLogService extends Service {

    private AsyncTask<String, Void, Integer> mAsyncTask ;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(AnalysisLogUtil.TAG, "AnalysisLogService onStartCommand");
        SharedPreferences settings = getSharedPreferences(AnalysisLogUtil.Preferences.PREFS_NAME, Context.MODE_PRIVATE);

        // 直前の非同期処理が終わってないこともあるのでキャンセルしておく
        if (mAsyncTask != null) mAsyncTask.cancel(true);

        mAsyncTask = new AnalysisLogAsyncTask(this) {
            @Override
            protected void onPostExecute(Integer result) {
                if(result == 0) {
                    final String filePath = getCacheDir().getPath() + "/" + AnalysisLogUtil.UNCATCHED_FILE_NAME;
                    final File file = new File(filePath);
                    if(file.delete()) {
                        Log.d(AnalysisLogUtil.TAG, "AnalysisLogService delete file = " + filePath);
                    }
                }
                else {
                    Log.d(AnalysisLogUtil.TAG, "AnalysisLogService delete file failed.");
                }
                stopSelf();
            }

            @Override
            protected void onCancelled(Integer integer) {
                stopSelf();
            }
        }.execute(AnalysisLogUtil.DEST_SERVER, settings.getString(AnalysisLogUtil.Preferences.FILE_PATH, null));
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Log.d(AnalysisLogUtil.TAG, "AnalysisLogService onDestroy");
        if (mAsyncTask != null) mAsyncTask.cancel(true);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

}
