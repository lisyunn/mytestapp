package AnalysisLog;

import android.annotation.SuppressLint;
import android.content.AsyncQueryHandler;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import java.nio.charset.Charset;

@SuppressLint("HandlerLeak")
class AnalysisLogTable  {
    private static final Uri URI = Uri.parse("content://analysislog.provider/");
    private Context mContext;
    private String mStackTraceData;
    private int mStackTraceSize;

    AnalysisLogTable(Context context) {
        mContext = context;
    }

    /**
     * データを取得する
     */
    public void load()
    {
        if(mContext == null) {
            Log.e(AnalysisLogUtil.TAG, "AnalysisLogTable load: mContext is null");
            return;
        }
        final AsyncQueryHandler handler = new AsyncQueryHandler(mContext.getContentResolver()) {
            @Override
            protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
                super.onQueryComplete(token, cookie, cursor);

                mStackTraceData = "";
                mStackTraceSize = 0;

                if (cursor != null) {
                    for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                        mStackTraceData += cursor.getString(cursor.getColumnIndex(AnalysisLogHelper.DATA));
                        mStackTraceSize += cursor.getInt(cursor.getColumnIndex(AnalysisLogHelper.SIZE));
                    }
                    cursor.close();
                }
                Log.d(AnalysisLogUtil.TAG, "AnalysisLogTable mStackTraceSize = " + mStackTraceSize);
                if(mStackTraceSize < AnalysisLogUtil.SEND_LOG_MAX_LEN) {
                    return;
                }
                new AnalysisLogAsyncTask(mContext) {
                    @Override
                    protected void onPostExecute(Integer result) {
                        if(result == 0) {
                            Log.d(AnalysisLogUtil.TAG, "AnalysisLogTable load success.");
                            delete();
                        }
                        else {
                            Log.d(AnalysisLogUtil.TAG, "AnalysisLogTable load failed.");
                        }
                    }

                    @Override
                    protected void onCancelled(Integer integer) {
                        Log.d(AnalysisLogUtil.TAG, "AnalysisLogTable load cancelled.");
                    }
                }.execute(AnalysisLogUtil.DEST_SERVER, null, null);
            }
        };
        handler.startQuery(0, null, URI, null, null, null, null);
    }

    /**
     * データを保存する
     */
    void store(String data, String errorCode) {
        if(mContext == null) {
            Log.e(AnalysisLogUtil.TAG, "AnalysisLogTable store: mContext is null");
            return;
        }
        ContentValues contentValues = new ContentValues();
        // replace <TAB> to 4 spaces
        data = data.replaceAll(AnalysisLogUtil.TAB, AnalysisLogUtil.SPACE4);
        final String parameters = AnalysisLogUtil.getSendLogRecode(mContext, errorCode);
        contentValues.put(AnalysisLogHelper.DATA, parameters + data + AnalysisLogUtil.CRLF2);
        contentValues.put(AnalysisLogHelper.SIZE, getByteLength(data));
        final AsyncQueryHandler handler = new AsyncQueryHandler(mContext.getContentResolver()) {
            @Override
            protected void onInsertComplete(int token, Object cookie, Uri uri) {
                super.onInsertComplete(token, cookie, uri);
                load();
            }
        };
        handler.startInsert(0, null, URI, contentValues);
    }

    /**
     * データを削除する
     */
    public void delete() {
        if(mContext == null) {
            Log.e(AnalysisLogUtil.TAG, "AnalysisLogTable delete: mContext is null");
            return;
        }
        final AsyncQueryHandler handler = new AsyncQueryHandler(mContext.getContentResolver()) {
            @Override
            protected void onDeleteComplete(int token, Object cookie, int result) {
                super.onDeleteComplete(token, cookie, result);
                Log.d(AnalysisLogUtil.TAG, "AnalysisLogTable delete result = " + result);
            }
        };
        handler.startDelete(0, null, URI, null, null);
    }

    /**
     * helper経由でdbをcloseする
     */
    public void close() {
        if(mContext == null) {
            Log.e(AnalysisLogUtil.TAG, "AnalysisLogTable close: mContext is null");
            return;
        }
        AnalysisLogHelper.getInstance(mContext).close();
    }

    String getStackTraceData() {
        return mStackTraceData;
    }

    private int getByteLength(String string) {
        if(string == null || string.isEmpty()) {
            return 0;
        }
        return string.getBytes(Charset.forName("UTF-8")).length;
    }

}
