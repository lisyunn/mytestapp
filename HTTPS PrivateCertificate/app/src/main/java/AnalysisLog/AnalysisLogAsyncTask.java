package AnalysisLog;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

/**
 * リクエストメッセージ（１レコード）
 * ver=1.0&ts=[yyyyMMddHHmmssfff]&dev=[端末識別子]&app=[アプリバージョン]&dn=[機種名]&type=L04&data=E0001_-1<TAB>[エラー詳細]
 *
 * [エラー詳細]（スタックトレース等）中の<TAB>は半角スペース４つに、<CR>及び<CR><LF>は<LF>に置換され
 * 複数レコードを送信する際、<CR><LF><CR><LF>をセパレータに用いる
 * 長さに上限は設けない
 */
abstract class AnalysisLogAsyncTask extends AsyncTask<String, Void, Integer> {

    private Context mContext;

    AnalysisLogAsyncTask(Context context) {
        mContext = context;
    }

    @Override
    protected Integer doInBackground(String... params) {

        OutputStream os = null;
        HttpURLConnection urlCon = null;
        int result = -1;

        try {
            final URL url = new URL(params[0]);

            String postData = null;
            if(params[1] != null) {
                postData = readFile(params[1]);
            }
            else {
                if(AnalysisLogUtil.sAnalysisLogTable != null) {
                    postData = AnalysisLogUtil.sAnalysisLogTable.getStackTraceData();
                }
            }

            if(postData == null || postData.isEmpty()) {
                Log.e( AnalysisLogUtil.TAG, " AnalysisLogAsyncTask postData is Empty.");
                return result;
            }

            if (postData.endsWith(AnalysisLogUtil.CRLF2)) {
                postData = postData.substring(0, postData.length() - AnalysisLogUtil.CRLF2.length());
            }

            urlCon = (HttpURLConnection) url.openConnection();
            //urlCon.setReadTimeout(5000);
            urlCon.setConnectTimeout(5000);
            urlCon.setRequestMethod("POST");
            urlCon.setDoInput(true);
            urlCon.setDoOutput(true);
            urlCon.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlCon.setUseCaches(false);
            if(mContext != null) {
                urlCon.setRequestProperty("User-Agent", AnalysisLogUtil.getSendLogUA(mContext));
                Log.d( AnalysisLogUtil.TAG, " AnalysisLogAsyncTask set User-Agent = " + urlCon.getRequestProperty("User-Agent"));
            }
            urlCon.connect();

            // データを送信する
            os = urlCon.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(postData);
            writer.flush();
            writer.close();
            os.close();

            int status = urlCon.getResponseCode();

            switch (status) {
                case HttpURLConnection.HTTP_OK:
                    result = 0;
                    break;
                case HttpURLConnection.HTTP_UNAUTHORIZED:
                    break;
                default:
                    break;
            }
            Log.d(AnalysisLogUtil.TAG , " AnalysisLogAsyncTask status = " + status );
        } catch (Exception e) {
            Log.e(AnalysisLogUtil.TAG, " " + e.getMessage() + " " + e.toString() );
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
                if (urlCon != null) {
                    urlCon.disconnect();
                }
            } catch (IOException e) {
                Log.e(AnalysisLogUtil.TAG, " " +e.getMessage() );
            }
        }
        Log.d( AnalysisLogUtil.TAG , " AnalysisLogAsyncTask End ");
        return result;
    }

    /**
     * キャッシュファイルの読み込み
     * 推薦：doInBackgroundで呼び出す
     * @param filePath File path
     * @return string
     */
    private String readFile(String filePath) {
        Scanner scanner = null;
        String text = "";
        try {
            scanner = new Scanner( new File(filePath));
            text = scanner.useDelimiter("\\A").next();
        } catch (Exception e) {
            Log.e(AnalysisLogUtil.TAG, e.getMessage());
        }
        finally {
            try {
                if(scanner != null) {
                    scanner.close();
                }
            } catch (Exception e) {
                Log.e(AnalysisLogUtil.TAG, e.getMessage());
            }
        }
        return text;
    }
}