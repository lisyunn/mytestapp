package AnalysisLog;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

public class AnalysisLogProvider  extends ContentProvider {

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public boolean onCreate() {
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        AnalysisLogHelper helper = AnalysisLogHelper.getInstance(getContext());
        SQLiteDatabase sdb = helper.getReadableDatabase();
        return sdb.query(
                AnalysisLogHelper.TABLENAME,
                new String[]{AnalysisLogHelper.ID, AnalysisLogHelper.SIZE,  AnalysisLogHelper.DATA},
                selection,
                selectionArgs,
                null,
                null,
                sortOrder,
                null
        );
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long rowId;
        AnalysisLogHelper helper = AnalysisLogHelper.getInstance(getContext());
        SQLiteDatabase sdb = helper.getWritableDatabase();
        rowId = sdb.insert(AnalysisLogHelper.TABLENAME, null, values);
        if (rowId == -1) {
            Log.e(AnalysisLogUtil.TAG, "couldn't insert into contents database");
            return null;
        }
        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return ContentUris.withAppendedId(uri, rowId);
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        AnalysisLogHelper helper = AnalysisLogHelper.getInstance(getContext());
        SQLiteDatabase sdb = helper.getWritableDatabase();
        int rows = sdb.update(AnalysisLogHelper.TABLENAME, values, selection, selectionArgs);
        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rows;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        AnalysisLogHelper helper = AnalysisLogHelper.getInstance(getContext());
        SQLiteDatabase sdb = helper.getWritableDatabase();
        int rows = sdb.delete(AnalysisLogHelper.TABLENAME, selection, selectionArgs);
        if (getContext() != null) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return rows;
    }

}