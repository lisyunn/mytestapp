package sslcert.ocsp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * OCSPのレスポンス結果保管クラス.
 * This is a cache to store OCSP responses
 */
class OcspCache {
    private static File sCacheDir = null;
    private static final int MAX_CACHE_EXPIRE_PERIOD = 7*24*60*60*1000; // one week
    private static final int DEFAULT_CACHE_EXPIRE_PERIOD = 24*60*60*1000;// one day
    private static final int CACHE_CLEANUP_THRESHOLD = 100;
    private static final int CACHE_CLEANUP_COUNT = 50;
    private static final String CACHE_INFO_DELIMITER = "_";
    private static final String CACHE_INFO_SPLITTER_REGEX = "[\\._]";
    private static final int CACHE_INFO_POSITION_STATUS = 2;
    private static final int CACHE_INFO_POSITION_THIS_UPDATE = 3;
    private static final int CACHE_INFO_POSITION_NEXT_UPDATE = 4;
    private static final String CACHE_FILE_EXTENSION = ".ocsp";
    private static final FilenameFilter FILENAME_FILTER_ALL = new FilenameFilter() {
        public boolean accept(File file, String name) {
            return name.endsWith(CACHE_FILE_EXTENSION);
        }
    };
    private static final char[] DIGITS = {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
    };

    protected static void init(File cacheDir) {
        sCacheDir = cacheDir;
    }

    protected static boolean isInitialized() {
        return sCacheDir != null && sCacheDir.exists();
    }


    protected static String encodeHexString(byte[] data) {
        return new String(encodeHex(data));
    }

    private static char[] encodeHex(byte[] data) {
        int l = data.length;
        char[] out = new char[l << 1];
        int i = 0;
        int j = 0;
        for (; i < l; i++) {
            out[j++] = DIGITS[(240 & data[i]) >>> 4];
            out[j++] = DIGITS[15 & data[i]];
        }

        return out;
    }

    protected static String generateCacheKey(X509Certificate certificate) {
        try {
            final MessageDigest md = MessageDigest.getInstance("SHA-256");
            final Principal principal = certificate.getIssuerDN();
            md.update(principal.getName().getBytes("UTF-8"));
            final String dn_hash = encodeHexString(md.digest());
            final String serial_num = certificate.getSerialNumber().toString(16);
            final String res = dn_hash + CACHE_INFO_DELIMITER + serial_num;
            OcspLog.d("CacheKey : " + res);
            return res;
        } catch (NoSuchAlgorithmException e) {
            OcspLog.e("Internal error : SHA-256 algorithm not found. " + e.getMessage());
            return null;
        } catch (UnsupportedEncodingException e) {
            OcspLog.e("Internal error : Failed  to UTF-8 encoded. " + e.getMessage());
            return null;
        }
    }

    protected static String generateCacheKeyByString(String string) {
        final String host_hash = getHexString(string);
        final String hostname = "hostname";
        final String res = host_hash + CACHE_INFO_DELIMITER + hostname;
        OcspLog.d("CacheKey : " + res);
        return res;
    }

    protected static int verifyCertFromCache(String cacheKey, String targetHost) {
        File cacheFile = getCacheFile(cacheKey, false);
        if(null != cacheFile) {
            OcspLog.d("Cache found. " + cacheFile.getName());
            CacheInfo cacheInfo = new CacheInfo(cacheFile.getName());
            long now = System.currentTimeMillis();
            if(now > cacheInfo.getThisUpdate() && now < cacheInfo.getNextUpdate()) {
                cacheFile.setLastModified(now);
                fileWrite(cacheFile, targetHost);
                OcspLog.d("Cache is valid. status : " + cacheInfo.getStatus());
                return cacheInfo.getStatus();
            } else {
                OcspLog.d("Cache is expired. (delete) " + cacheFile.getName());
                cacheFile.delete();
                return OcspVerifier.STATUS_NONE;
            }
        } else {
            OcspLog.d("No cache found.");
            return OcspVerifier.STATUS_NONE;
        }
    }

    protected static void createCache(String cacheKey, int status, Date thisUpdate, Date nextUpdate, String targetHost) {
        if(cacheKey == null) {
            OcspLog.d("Cachekey is null. No cache file created.");
        } else {
            long now = System.currentTimeMillis();

            if(thisUpdate == null || nextUpdate == null || now < thisUpdate.getTime() || now > nextUpdate.getTime()) {
                thisUpdate = new Date(now);
                nextUpdate = new Date(now + DEFAULT_CACHE_EXPIRE_PERIOD); // one day
            }
            OcspLog.d("thisUpdate : " + thisUpdate.toString());
            OcspLog.d("nextUpdate : " + nextUpdate.toString());

            final long weekAfter = now + MAX_CACHE_EXPIRE_PERIOD;
            StringBuilder cacheFileName = new StringBuilder();
            cacheFileName.append(cacheKey).append(CACHE_INFO_DELIMITER).append(String.valueOf(status)).append(CACHE_INFO_DELIMITER).append(String.valueOf(thisUpdate.getTime())).append(CACHE_INFO_DELIMITER);
            if(weekAfter < nextUpdate.getTime()) {
                cacheFileName.append(String.valueOf(weekAfter));
            } else {
                cacheFileName.append(String.valueOf(nextUpdate.getTime()));
            }
            cacheFileName.append(CACHE_FILE_EXTENSION);
            OcspLog.d("Cache file name : " + cacheFileName.toString());
            deleteCacheByKey(cacheKey);

            try {
                final String filePath = sCacheDir.getPath() + "/" + cacheFileName;
                File file = new File(filePath);
                file.createNewFile();
                fileWrite(file, targetHost);
                OcspLog.d("Cache file created. " + cacheFileName);
            } catch (NullPointerException e) {
                OcspLog.e("Failed to create cache file. " + e.getMessage());
            } catch (IOException e) {
                OcspLog.e("Failed to create cache file. " + e.getMessage());
            }
            shrinkCache();
        }
    }

    private static String getHexString(String targetHost) {
        String res;
        MessageDigest sha = null;
        try {
            sha = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            OcspLog.e("Internal error : SHA-256 algorithm not found. " + e.getMessage());
        }
        if(sha == null) {
            return targetHost;
        }

        res = encodeHexString(sha.digest(targetHost.getBytes()));

        return res;
    }

    protected static int verifyCertFromTargetHost(String targetHost) {
        if(targetHost == null) {
            return OcspVerifier.STATUS_NONE;
        }
        final String res = getHexString(targetHost);
        if(res == null) {
            return OcspVerifier.STATUS_NONE;
        }
        File cacheFile = getCacheFile(res, true);
        if(null != cacheFile) {
            OcspLog.d("TargetHost Cache found. " + cacheFile.getName());
            CacheInfo cacheInfo = new CacheInfo(cacheFile.getName());
            long now = System.currentTimeMillis();
            if(now > cacheInfo.getThisUpdate() && now < cacheInfo.getNextUpdate()) {
                cacheFile.setLastModified(now);
                OcspLog.d("TargetHost Cache is valid. status : " + cacheInfo.getStatus());
                return cacheInfo.getStatus();
            } else {
                OcspLog.d("TargetHost Cache is expired." + cacheFile.getName());
                if(cacheFile.getName().contains("hostname")) {
                    cacheFile.delete();
                }
                return OcspVerifier.STATUS_NONE;
            }
        } else {
            OcspLog.d("No TargetHost cache found.");
            return OcspVerifier.STATUS_NONE;
        }
    }

    private static void fileWrite(File file, String targetHost) {
        final String res = getHexString(targetHost);
        if(fileRead(file.getPath(), res)) {
            return;
        }
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
            pw.println(res);
        } catch (IOException e) {
            OcspLog.e("fileWrite" + e.getMessage());
        }
        finally {
            if(pw != null) {
                pw.close();
            }
        }

    }

    private static boolean fileRead(String filePath, String hostKey) {
        FileReader fr = null;
        BufferedReader br = null;
        try {
            fr = new FileReader(filePath);
            br = new BufferedReader(fr);

            String line;
            while ((line = br.readLine()) != null) {
                if(line.contentEquals(hostKey)) {
                    return true;
                }
            }
        } catch (FileNotFoundException e) {
            OcspLog.e("fileRead" + e.getMessage());
            return false;
        } catch (IOException e) {
            OcspLog.e("fileRead" + e.getMessage());
            return false;
        } finally {
            try {
                br.close();
                fr.close();
            } catch (IOException e) {
                OcspLog.e("fileRead" + e.getMessage());
                return false;
            }
        }
        return false;
    }

    private static File getCacheFile(String key, boolean isTargetHost) {
        File[] files;
        if(isTargetHost) {
            files = getCacheFilesByHostKey(key);
        }
        else {
            files = getCacheFilesByKey(key);
        }
        if(files == null) {
            OcspLog.d("Failed to get cache file. Please initialize library again.");
            return null;
        } else {
            switch(files.length) {
                case 0:
                    return null;
                case 1:
                    return files[0];
                default:
                    OcspLog.d("Deprecate cache files found.");
                    List fileList = Arrays.asList(files);
                    Collections.sort(fileList, new LongestLimitFileComparator());

                    for(int target = 1; target < fileList.size(); ++target) {
                        OcspLog.d("Delete old cache. " + ((File)fileList.get(target)).getName());
                        ((File)fileList.get(target)).delete();
                    }

                    return (File)fileList.get(0);
            }
        }
    }

    protected static void deleteCache() {
        File[] files = sCacheDir.listFiles(FILENAME_FILTER_ALL);
        if(files != null && files.length > 0) {
            for(int i = 0; i < files.length; ++i) {
                File file = files[i];
                OcspLog.d("Delete cache file. " + file.getName());
                file.delete();
            }
        }
    }

    private static void deleteCacheByKey(String cacheKey) {
        File[] files = getCacheFilesByKey(cacheKey);
        if(files != null && files.length > 0) {
            for(int i = 0; i < files.length; ++i) {
                File file = files[i];
                OcspLog.d("Delete cache file. " + file.getName());
                file.delete();
            }
        }

    }

    private static void shrinkCache() {
        File[] files = sCacheDir.listFiles(FILENAME_FILTER_ALL);
        if(files != null) {
            if(files.length > CACHE_CLEANUP_THRESHOLD) {
                OcspLog.d("The number of cache files exceeded a threshold.");
                List fileList = Arrays.asList(files);
                Collections.sort(fileList, new NewerFileComparator());

                for(int target = CACHE_CLEANUP_COUNT; target < fileList.size(); ++target) {
                    OcspLog.d("Delete cache file. " + ((File)fileList.get(target)).getName());
                    ((File)fileList.get(target)).delete();
                }
            }

        }
    }

    private static File[] getCacheFilesByKey(final String cacheKey) {
        FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File file, String name) {
                return name.startsWith(cacheKey);
            }
        };
        return sCacheDir.listFiles(filter);
    }

    private static File[] getCacheFilesByHostKey(final String hostKey) {
        FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File file, String name) {
                if(name.endsWith(CACHE_FILE_EXTENSION)) {
                    return fileRead(file.getPath() + "/" + name, hostKey);
                }
                else {
                    return false;
                }
            }
        };
        return sCacheDir.listFiles(filter);
    }

    /**
     *
     */
    private static class CacheInfo {
        private final int status;
        private final long thisUpdate;
        private final long nextUpdate;

        private CacheInfo(String cacheInfoStr) {
            String[] infoArray = cacheInfoStr.split(CACHE_INFO_SPLITTER_REGEX);
            this.status = Integer.parseInt(infoArray[CACHE_INFO_POSITION_STATUS]);
            this.thisUpdate = Long.parseLong(infoArray[CACHE_INFO_POSITION_THIS_UPDATE]);
            this.nextUpdate = Long.parseLong(infoArray[CACHE_INFO_POSITION_NEXT_UPDATE]);
        }

        private int getStatus() {
            return this.status;
        }

        private long getThisUpdate() {
            return this.thisUpdate;
        }

        private long getNextUpdate() {
            return this.nextUpdate;
        }
    }

    private static class LongestLimitFileComparator implements Comparator<File>, Serializable {
        @Override
        public int compare(File file1, File file2) {
            if(file1 != null && file2 != null) {
                CacheInfo info1 = new CacheInfo(file1.getName());
                CacheInfo info2 = new CacheInfo(file2.getName());
                return (int) Math.signum((float)(info2.getNextUpdate() - info1.getNextUpdate()));
            }
            return 0;
        }
    }

    private static class NewerFileComparator implements Comparator<File>, Serializable {
        @Override
        public int compare(File file1, File file2) {
            if(file1 != null && file2 != null) {
                return (int) Math.signum((float)(file2.lastModified() - file1.lastModified()));
            }
            return 0;
        }
    }

}
