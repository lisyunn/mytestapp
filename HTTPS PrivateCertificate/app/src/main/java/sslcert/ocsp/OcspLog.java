package sslcert.ocsp;

import android.util.Log;

import org.jssec.android.https.privatecertificate.BuildConfig;

class OcspLog {
    private static final boolean IS_LOGGED = BuildConfig.DEBUG;
    private static final String TAG = "OcspVerifier";

    protected static void v(String message) {
        if(!IS_LOGGED || message == null) {
            return;
        }
        Log.v(TAG, message);
    }

    protected static void d(String message) {
        if(!IS_LOGGED) {
            return;
        }
        Log.d(TAG, message);
    }

    protected static void i(String message) {
        if(!IS_LOGGED || message == null) {
            return;
        }
        Log.i(TAG, message);
    }

    protected static void w(String message) {
        if(!IS_LOGGED || message == null) {
            return;
        }
        Log.w(TAG, message);
    }

    public static void e(String message) {
        if(!IS_LOGGED || message == null) {
            return;
        }
        Log.e(TAG, message);
    }
}