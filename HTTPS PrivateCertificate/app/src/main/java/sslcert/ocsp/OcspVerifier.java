package sslcert.ocsp;

import android.content.Context;

import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.ASN1Sequence;
import org.bouncycastle.asn1.DERIA5String;
import org.bouncycastle.asn1.DEROctetString;
import org.bouncycastle.asn1.ocsp.OCSPResponseStatus;
import org.bouncycastle.asn1.x509.AccessDescription;
import org.bouncycastle.asn1.x509.AuthorityInformationAccess;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.X509ObjectIdentifiers;
import org.bouncycastle.cert.ocsp.BasicOCSPResp;
import org.bouncycastle.cert.ocsp.CertificateID;
import org.bouncycastle.cert.ocsp.CertificateStatus;
import org.bouncycastle.cert.ocsp.OCSPReq;
import org.bouncycastle.cert.ocsp.OCSPReqBuilder;
import org.bouncycastle.cert.ocsp.OCSPResp;
import org.bouncycastle.cert.ocsp.RevokedStatus;
import org.bouncycastle.cert.ocsp.SingleResp;
import org.bouncycastle.cert.ocsp.jcajce.JcaCertificateID;
import org.bouncycastle.operator.jcajce.JcaDigestCalculatorProviderBuilder;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.IDN;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.Date;

import javax.net.ssl.HttpsURLConnection;

/**
 * BouncyCastle APIを使って、OCSPレスポンスのデータから失効状態を取得
 * Used to check if a Certificate is revoked or not by its CA using Online Certificate
 * Status Protocol (OCSP).
 * Import bcprov-jdk15on-1.56.jar. Download from http://repo2.maven.org/maven2/org/bouncycastle/bcprov-jdk15on/1.56/
 * Import bcpkix-jdk15on-1.56.jar. Download from http://repo2.maven.org/maven2/org/bouncycastle/bcpkix-jdk15on/1.56/
 */
public class OcspVerifier {

    public static final int STATUS_GOOD = 0;    // サーバ証明書有効
    public static final int STATUS_REVOKED = 1; // サーバ証明書失効
    public static final int STATUS_NONE = 2;    // サーバ証明書不明
    private static int sConnectTimeout = 5000;
    private static int sReadTimeout = 5000;
    private static String sUserAgent = null;

    /**
     * OcspVerifier初期化処理
     * @param context
     * @throws OcspException
     */
    public static void init(Context context, String userAgent) throws OcspException {
        OcspLog.d("init() start");
        if(context != null) {
            OcspCache.init(context.getCacheDir());
            sUserAgent = userAgent;
            OcspLog.d("init() end");
        } else {
            OcspLog.d("Failed to initialize library.");
            throw new OcspException("Failed to initialize.");
        }
    }

    /**
     * OcspCache初期化状態取得
     * @return キャッシュが存在かどうか
     */
    private static boolean isInitialized() {
        return OcspCache.isInitialized();
    }

    /**
     * OCSPリクエスト送信に使用する接続タイムアウト値を設定する
     * @param timeout　ミリ秒単位の接続タイムアウト値
     * @throws OcspException
     */
    public static void setConnectTimeout(int timeout) throws OcspException {
        OcspLog.d("setConnectTimeout() start");
        OcspLog.d("Timeout : " + timeout);
        if(timeout < 0) {
            OcspLog.d("Connect timeout must be zero or higher.");
            throw new OcspException("Connect timeout must be zero or higher.");
        } else {
            sConnectTimeout = timeout;
            OcspLog.d("setConnectTimeout() end");
        }
    }

    /**
     * OCSPリクエスト送信に使用する読み取りタイムアウト値を設定する
     * @param timeout　ミリ秒単位の読み取りタイムアウト値
     * @throws OcspException
     */
    public static void setReadTimeout(int timeout) throws OcspException {
        OcspLog.d("setReadTimeout() start");
        OcspLog.d("Timeout : " + timeout);
        if(timeout < 0) {
            OcspLog.d("Read timeout must be zero or higher.");
            throw new OcspException("Read timeout must be zero or higher.");
        } else {
            sReadTimeout = timeout;
            OcspLog.d("setReadTimeout() end");
        }
    }

    /**
     * 指定されたURLで使用されているサーバ証明書の失効確認を行う
     * 指定されたURLのプロトコルがhttps以外(http/file/ftp)の場合は、無条件にSTATUS_GOOD が返却される
     * 本メソッドは内部で通信処理を行うため、非UIスレッドにて実行する必要がある
     * @param targetUrlString 失効確認対象のURL
     * @param useCache キャッシュを利用するかどうか
     * @return サーバ証明書の失効状態
     * @throws OcspException
     */
    public static int verifyUrl(String targetUrlString, boolean useCache) throws OcspException {
        int result = STATUS_NONE;
        if(!isInitialized()) {
            OcspLog.d("OcspVerifier has not been initialized.");
            throw new OcspException("OcspVerifier has not been initialized.");
        } else {
            String targetHost;
            URL targetUrl;
            try {
                targetUrl = new URL(targetUrlString);
                // 正規url:スキーマ、ホスト名、ポート番号のみからなるURLを変更
                targetUrl = new URL(targetUrl.getProtocol(), IDN.toASCII(targetUrl.getHost()), targetUrl.getPort(), "");
                targetHost = targetUrl.toString();
            } catch (MalformedURLException e) {
                OcspLog.e("URL is malformed. " + e.getMessage());
                throw new OcspException("URL is malformed.", e);
            }

            if(!targetUrl.getProtocol().equals("https")) {
                OcspLog.d("Target protocol is " + targetUrl.getProtocol() + ". Skip verify.");
                return result;
            } else {
                HttpsURLConnection connection = null;
                try {
                    OcspLog.d("Target URL : " + targetUrlString);
                    OcspLog.d("Target Host : " + targetHost);
                    if(useCache) {
                        int statue = OcspCache.verifyCertFromTargetHost(targetHost);
                        switch(statue) {
                            case STATUS_GOOD:
                                result = STATUS_GOOD;
                                return result;
                            case STATUS_REVOKED:
                                result = STATUS_REVOKED;
                                return result;
                            default:
                        }
                    }
                    else {
                        OcspLog.d("useCache : " + useCache);
                    }
                    connection = (HttpsURLConnection) targetUrl.openConnection();
                    connection.setInstanceFollowRedirects(false);
                    connection.setConnectTimeout(sConnectTimeout);
                    OcspLog.d("Get server certificates connect timeout : " + connection.getConnectTimeout());
                    connection.setReadTimeout(sReadTimeout);
                    OcspLog.d("Get server certificates read timeout : " + connection.getReadTimeout());
                    if(sUserAgent != null) {
                        connection.setRequestProperty("User-Agent", sUserAgent);
                        OcspLog.d("Get server certificates set User-Agent : " + connection.getRequestProperty("User-Agent"));
                    }
                    connection.connect();
                    connection.getResponseCode();
                    Certificate[] serverCerts = connection.getServerCertificates();
                    X509Certificate peerX509cert = (X509Certificate) serverCerts[0];
                    X509Certificate caX509cert = (X509Certificate) serverCerts[1];
                    int ret = verifyCert(peerX509cert, caX509cert, useCache, targetHost);;
                    result = ret;
                } catch (Exception e) {
                    OcspCache.createCache(OcspCache.generateCacheKeyByString(targetHost), (result == STATUS_NONE ? STATUS_GOOD : result), null, null, targetHost);
                    OcspLog.e("Failed to get server certificates. " + e.getMessage());
                    throw new OcspException("Failed to get server certificates.", e);
                } finally {
                    if(connection != null) {
                        connection.disconnect();
                    }
                    return result;
                }
            }
        }
    }

    /**
     * 指定されたサーバ証明書の失効確認を行う
     * 本メソッドは内部で通信処理を行うため、非UIスレッドにて実行する必要がある
     * @param peerCert 失効確認対象の証明書
     * @param rootCert 失効確認対象の証明書の発行者の証明書
     * @param useCache キャッシュを利用するかどうか
     * @param targetHost 失効確認対象のスキーマ、ホスト名、ポート番号のみからなるURL
     * @return サーバ証明書の失効状態
     * @throws OcspException
     */
    public static int verifyCert(X509Certificate peerCert, X509Certificate rootCert, boolean useCache, String targetHost) throws OcspException {
        OcspLog.d("Target certificate serial number : " + peerCert.getSerialNumber().toString(16));
        OcspLog.d("useCache : " + useCache);
        if(!isInitialized()) {
            OcspLog.d("OcspVerifier has not been initialized.");
            throw new OcspException("OcspVerifier has not been initialized.");
        } else if(targetHost == null) {
            OcspLog.d("verifyCert targetHost should not be null.");
            throw new OcspException("verifyCert targetHost should not be null.");
        } else {
            String cacheKey = OcspCache.generateCacheKey(peerCert);
            if(useCache && cacheKey != null) {
                int url = OcspCache.verifyCertFromCache(cacheKey, targetHost);
                switch(url) {
                    case STATUS_GOOD:
                        return STATUS_GOOD;
                    case STATUS_REVOKED:
                        return STATUS_REVOKED;
                    default:
                }
            }
            try {
                return checkRevocationStatus(peerCert, rootCert, cacheKey, targetHost);
            } catch (OcspException e) {
                OcspLog.e("OcspVerifier checkRevocationStatus failed.");
            }
            return STATUS_NONE;
        }
    }

    /**
     * サーバ証明書の失効確認結果キャッシュを削除する
     * 既存のキャッシュを削除する特別な事情がある場合のみを使用
     */
    public static void deleteCache() {
        OcspLog.d("deleteCache() start");
        if(!isInitialized()) {
            OcspLog.d("OcspUtil has not been initialized. No cache file deleted.");
        } else {
            OcspCache.deleteCache();
            OcspLog.d("deleteCache() end");
        }
    }


    /**
     * This method generates an OCSP Request to be sent to an OCSP endpoint.
     *
     * @param issuerCert   is the Certificate of the Issuer of the peer certificate we are interested in.
     * @param serialNumber of the peer certificate.
     * @return generated OCSP request.
     * @throws OcspException
     */
    private static OCSPReq generateOCSPRequest(X509Certificate issuerCert, BigInteger serialNumber)
            throws OcspException {

        //Add provider BC
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        try {
            // Generate the id for the certificate we are looking for
            JcaCertificateID id = new JcaCertificateID(
                    new JcaDigestCalculatorProviderBuilder().setProvider("BC").build().get(CertificateID.HASH_SHA1),
                    issuerCert, serialNumber);

            // basic request generation with nonce
            OCSPReqBuilder gen = new OCSPReqBuilder();

            gen.addRequest(id);

            return gen.build();
        }
        catch (Exception e) {
            throw new OcspException("Cannot generate OSCP Request with the given certificate",e);
        }
    }

    /**
     * Gets an ASN.1 encoded OCSP response (as defined in RFC 2560) from the given service URL. Currently supports
     * only HTTP.
     *
     * @param serviceUrl URL of the OCSP endpoint.
     * @param request    an OCSP request object.
     * @return OCSP response encoded in ASN.1 structure.
     * @throws OcspException
     *
     */
    private static OCSPResp getOCSPResponse(String serviceUrl, OCSPReq request) throws OcspException {
        HttpURLConnection con = null;
        try {
            byte[] array = request.getEncoded();
            if (serviceUrl.startsWith("http")) {
                URL url = new URL(serviceUrl);
                con = (HttpURLConnection) url.openConnection();
                con.setRequestProperty("Content-Type", "application/ocsp-request");
                con.setRequestProperty("Accept", "application/ocsp-response");
                con.setDoOutput(true);
                con.setConnectTimeout(sConnectTimeout);
                con.setReadTimeout(sReadTimeout);
                OutputStream out = con.getOutputStream();
                DataOutputStream dataOut = new DataOutputStream(new BufferedOutputStream(out));
                dataOut.write(array);

                dataOut.flush();
                dataOut.close();

                //Check errors in response:
                if (con.getResponseCode() / 100 != 2) {
                    OcspLog.e("Error getting ocsp response." + "Response code is " + con.getResponseCode());
                    return null;
                }

                //Get Response
                InputStream in = (InputStream) con.getContent();
                return new OCSPResp(in);
            } else {
                OcspLog.e("Only http is supported for ocsp calls");
                return null;
            }
        } catch (IOException e) {
            OcspLog.e("Cannot get ocspResponse from url: " + serviceUrl + " " + e);
            return null;
        }
        finally {
            if(con != null) {
                con.disconnect();
            }
        }
    }


    /**
     * Gets the revocation status (Good, Revoked or Unknown) of the given peer certificate.
     *
     * @param peerCert   The certificate we want to check if revoked.
     * @param issuerCert Needed to create OCSP request.
     * @param cacheKey  Needed to create OcspCache.
     * @param targetHost The Target Host
     * @return revocation status of the peer certificate.
     * @throws OcspException
     *
     */
    private static int checkRevocationStatus(X509Certificate peerCert, X509Certificate issuerCert, String cacheKey, String targetHost)
            throws OcspException {
        int result = STATUS_NONE;
        Date mThisUpdate = null;
        Date mNextUpdate = null;
        try {

            OCSPReq request = generateOCSPRequest(issuerCert, peerCert.getSerialNumber());
            String serviceUrl = getOcspServerUrl(peerCert);
            if (serviceUrl != null) {
                OcspLog.d("OCSP responder URL : " + serviceUrl);
                OCSPResp ocspResponse = getOCSPResponse(serviceUrl, request);
                if(OCSPResponseStatus.SUCCESSFUL==ocspResponse.getStatus())
                    OcspLog.d("server gave response fine");

                BasicOCSPResp basicResponse = (BasicOCSPResp) ocspResponse.getResponseObject();
                SingleResp[] responses = (basicResponse==null) ? null : basicResponse.getResponses();

                if (responses!=null && responses.length == 1) {
                    SingleResp resp = responses[0];
                    mThisUpdate = resp.getThisUpdate();
                    mNextUpdate = resp.getNextUpdate();
                    Object status = resp.getCertStatus();
                    if (status == CertificateStatus.GOOD) {
                        OcspLog.d("OCSP Status is good!");
                        result = STATUS_GOOD;
                    } else if (status instanceof RevokedStatus) {
                        OcspLog.d("OCSP Status is revoked!");
                        result = STATUS_REVOKED;
                    }  else {
                        OcspLog.d("OCSP Status is unknown!");
                    }
                }
            }
        }
        catch (Exception e) {
            throw new OcspException(e);
        }
        finally {
            OcspCache.createCache(cacheKey, (result == STATUS_NONE ? STATUS_GOOD : result), mThisUpdate, mNextUpdate, targetHost);
            return result;
        }
    }

    /**
     * Get OCSP responder URL
     *
     * @param cert is the certificate
     * @return OCSP endpoint URL
     * @throws OcspException
     *
     */
    private static String getOcspServerUrl(X509Certificate cert) throws OcspException {

        //Gets the DER-encoded OCTET string for the extension value for Authority information access Points
        byte[] aiaExtensionValue = cert.getExtensionValue(Extension.authorityInfoAccess.getId());
        if (aiaExtensionValue == null) {
            throw new OcspException("Certificate doesn't have authority " +
                    "information access points");
        }
        //might have to pass an ByteArrayInputStream(aiaExtensionValue)
        ASN1InputStream asn1In = new ASN1InputStream(aiaExtensionValue);
        AuthorityInformationAccess authorityInformationAccess;

        try {
            DEROctetString aiaDEROctetString = (DEROctetString) (asn1In.readObject());
            ASN1InputStream asn1InOctets = new ASN1InputStream(aiaDEROctetString.getOctets());
            ASN1Sequence aiaASN1Sequence = (ASN1Sequence) asn1InOctets.readObject();
            authorityInformationAccess = AuthorityInformationAccess.getInstance(aiaASN1Sequence);
        } catch (IOException e) {
            throw new OcspException("Cannot read certificate to get OCSP URLs", e);
        }

        AccessDescription[] accessDescriptions = authorityInformationAccess.getAccessDescriptions();
        for (AccessDescription accessDescription : accessDescriptions) {
            GeneralName gn = accessDescription.getAccessLocation();
            if (gn.getTagNo() == GeneralName.uniformResourceIdentifier && X509ObjectIdentifiers.ocspAccessMethod.getId().equals(accessDescription.getAccessMethod().getId())) {
                DERIA5String str = DERIA5String.getInstance(gn.getName());
                return str.getString();
            }
        }
        throw new OcspException("Cant get OCSP urls from certificate");
    }
}