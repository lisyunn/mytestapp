package sslcert.ocsp;

/**
 * This class wraps an exception that could be thrown during the OCSP certificate
 * verification process.
 */
public class OcspException extends Exception {

    public OcspException(String message) {
        super(message);
    }

    public OcspException(Throwable throwable) {
        super(throwable);
    }

    public OcspException(String message, Throwable throwable) {
        super(message, throwable);
    }
}